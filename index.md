# Gitlab Page 1 Markdown

~~~cpp
auto count = GetCount(L"SMO");
~~~

# HttpServer

Dies ist erforderlich, da Methoden HttpClient gleichzeitig aufgerufen werden können und eine Garantie für die Threadsicherheit benötigen. Wenn einem Handler eine HttpClient Instanz zugewiesen wird, wird die SendAsync Methode des Handlers möglicherweise gleichzeitig von der HttpClient Instanz aufgerufen und muss threadsicher sein.

![Alt text](bild.jpg)

Created by *Mathias Rentsch*


in November 2023



# HttpClient

Stellt eine Klasse zum Senden von HTTP-Anforderungen und zum Empfangen von HTTP-Antworten von einer Ressource bereit, die durch einen URI identifiziert wird.

## Hinweis

Bitte nur in einem geheiztem Büro verwenden.

## Beispiel

~~~cpp
auto GetCount(std::wstring const & department) -> int
{
    return std::random(100);
}
~~~

## Compiler

|Produkt|Version|
|-|-|
|gcc|7.0,8.0|
|clang|11,12|
|mcc|5.5|


*This Page created by Mathias Rentsch*  
*in November 2023*


