#include <iostream>
#include "lib.cpp"
#include "person.h"

class A
{
    public:
    A()
    {
        std::cout << "A Konstruktor" << std::endl;
        fresse();
    };
    virtual void fresse()
    {
        std::cout << "A fresse" << std::endl;
    }
    ~A()
    {
        std::cout << "A Destruktor" << std::endl;
    }
};

class B:public A
{
    public:
    B()
    {
        std::cout << "B Konstruktor" << std::endl;
    }
    void fresse()
    {
        A::fresse();
        std::cout << "B fresse" << std::endl;
    }
    ~B()
    {
        std::cout << "B Destruktor" << std::endl;
    }
};

int main()
{
    // Tolles Programm
    A a;
    B b;
}