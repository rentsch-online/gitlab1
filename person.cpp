#include <iostream>
#include "person.h"

Person::Person()
{
    std::cout << "Constructor" << std::endl;
}   
Person::Person(const Person &p)
{
    std::cout << "CopyConstructor" << std::endl;
}
Person::Person(Person &&p)
{
     std::cout << "MoveConstructor" << std::endl;
}
Person&Person::operator=(const Person &p)
{
    std::cout << "CopyAssignment" << std::endl;
    return *this;
}
Person&Person::operator=(Person &&p)
{
    std::cout << "MoveAssignment" << std::endl;
    return *this;
}
Person::~Person()
{
     std::cout << "Destructor" << std::endl;
};