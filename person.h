// Erster Versuch die Gitlab-Web-IDE zu verwenden
// Mathias Rentsch
// 17.11.2023

#include <string>
class Person
{
    std::string name;
    int age;

public:
    Person();                           // Ctor
    Person(const Person &p);            // 1/5: Copy Ctor
    Person(Person &&p);                 // 4/5: Move Ctor
    Person& operator=(const Person &p); // 2/5: Copy Assignment
    Person& operator=(Person &&p);      // 5/5: Move Assignment
    ~Person();                          // 3/5: Dtor
};