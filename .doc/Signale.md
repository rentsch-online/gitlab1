# Signale

Das Ks-Signalsystem (Ks steht für Kombinationssignal) wird seit 1994 bei der Deutschen Bahn AG (vorher Deutsche Bundesbahn und Deutsche Reichsbahn) verwendet. Es kommen Signale zum Einsatz, die Vor- und/oder Hauptsignalfunktion in einem Signalschirm vereinen. Sie ersetzen nach und nach im Rahmen von Stellwerksneubauten die alten H/V-Licht- und Formsignale, die Hl-Signale sowie die Sv-Signale. Ihr Einsatzgebiet ist auf elektronische Stellwerke (ESTW) beschränkt, da die Entwicklung entsprechender Relaisbaugruppen als nicht wirtschaftlich betrachtet wurde.

![KS-Signal](220px-Ks_Signal_NALB.jpg)

## Geschichte
Im Zuge der deutschen Wiedervereinigung und der damit verbundenen Zusammenlegung der Deutschen Bundesbahn mit der Deutschen Reichsbahn entstand die Notwendigkeit, die in Ost und West unterschiedlichen Signalsysteme zu vereinheitlichen.

Das bei der Bundesbahn übliche H/V-Signalsystem und das bei der Deutschen Reichsbahn verwendete Hl-Signalsystem waren allerdings nicht zueinander kompatibel, sodass eine zeitnahe Umstellung aller Signale in einem Teil Deutschlands technisch schwierig war.

1987 gründete die Deutsche Bundesbahn eine Arbeitsgruppe, um die erweiterten Möglichkeiten der neu errichteten elektronischen Stellwerke (ESTW) besser zu nutzen. Das Ks-Signalsystem war daher von vornherein nur für den Einsatz bei ESTW vorgesehen. Eine Rückportierung auf Relaisstellwerke war nicht angedacht. Die Arbeitsgruppe erarbeitete ihren Vorschlag dabei auf der Basis des auf der Strecke Augsburg–Donauwörth erprobten neuartigen Sk-Signalsystems.

Die Vorstände beider deutscher Staatsbahnen beschlossen im März 1991 die Einführung dieses neuen Signalsystems. Es wurde dabei Ks-Signalsystem genannt, weil Vor- und Hauptsignal in einem Signal kombiniert werden konnten. Die Bezeichnung ist nicht mit den ebenfalls bei der Deutschen Bundesbahn eingeführten Kompaktsignalen (KS) zu verwechseln, welche einen neuartigen Signalschirm für H/V-Signale darstellen und den Ks-Signalen optisch ähneln.

Der erste mit Ks-Signalen ausgestattete Streckenabschnitt Magdeburg–Sudenburg–Marienborn wurde 1993 innerhalb des Steuerbezirkes des elektronischen Stellwerkes Eilsleben durch die Deutsche Reichsbahn in Betrieb genommen. Es war gleichzeitig das erste ESTW der Deutschen Reichsbahn, während die Deutsche Bundesbahn bereits zuvor ESTW – jedoch mit H/V-Signalen – in Betrieb genommen hatte.

Rechtlich sind Ks-Signale bis heute kein Teil der Eisenbahn-Signalordnung (ESO). Sie wurden allerdings als Betriebsversuch mit Bescheid (EBA Pr.2412 Aks521.doc Frau Rudolph) zur ESO des Eisenbahn-Bundesamtes vom 30. November 1995 bis zum Inkrafttreten der 4. Änderungsverordnung zur ESO genehmigt.[1] Die genannte Änderungsverordnung wurde jedoch nie erlassen, sodass der Betriebsversuch bis heute andauert.

Mit Inbetriebnahme erster Streckenabschnitte mit ETCS Level 2 ohne Signale (L2oS) wurden ab 2015 erstmals Zufahrtsicherungssignale eingeführt. Wenn nach einem Zufahrtsicherungssignal keine Fahrwegverzweigung mehr zu einer nicht mit L2oS ausgerüsteten Strecke besteht, können diese Ks-Signale nicht den Signalbegriff „Fahrt“ zeigen und sind bei regulären Zugfahrten dunkelgeschaltet.